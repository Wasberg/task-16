let cash = 0;
let days = 1;
let weeks = 1;
let years = 1;

const workBtn = document.getElementById("workBtn");
const loanBtn = document.getElementById("loanBtn");
const buyBtn = document.getElementById("buyBtn");
const restart = document.getElementById("restart");
 
let selected = document.getElementById("computer-select");

workBtn.addEventListener("click", function(){
    cash = cash + 10;
    document.getElementById("krs").innerHTML = cash;
    days++;
    document.getElementById("days").innerHTML = days;
    if (days % 5 === 0 + 1) {
        weeks++;
        document.getElementById("weeks").innerHTML = weeks;
    }
    if (weeks % 52 === 0) {
        years++;
        document.getElementById("years").innerHTML = years;
    }
});

loanBtn.addEventListener("click", function(){
    const loan = Math.random();
    document.getElementById("loanBtn").disabled = true;
    if (loan <= 0.5) {
        cash = cash + 6000;
        document.getElementById("krs").innerHTML = cash;
        document.getElementById("bank-message").innerHTML = "Okay sir, here you go. Good luck!";
    } else {
        document.getElementById("bank-message").innerHTML = "You already have too many loans! GET OUT OF HERE!";
    }
});

buyBtn.addEventListener("click", function(){
    if (cash < selected.value) {
        document.getElementById("store-message").innerHTML = "Sorry sir you can't buy that computer yet....";
        restart.addEventListener("click", function(){
            document.getElementById("store-message").innerHTML = "Welcome to the store. Here you can purchase your new computer!";
        });
    } else {
        document.getElementById("store-message").innerHTML ="You win!";
        document.getElementById("restart").innerHTML ="Restart Game."
        document.getElementById("buyBtn").disabled = true;
        document.getElementById("smRestart").disabled = true;
        restart.addEventListener("click", function(){
            location.reload(false);
        });
    }
});